// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Lab5GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class LAB5_API ALab5GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
